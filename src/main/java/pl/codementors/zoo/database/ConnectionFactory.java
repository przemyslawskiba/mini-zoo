package pl.codementors.zoo.database;

import org.apache.commons.dbcp2.BasicDataSource;
import org.apache.commons.dbcp2.PoolingDataSource;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

/**
 * Created by psysiu on 6/28/17.
 */
public class ConnectionFactory {

    private final static String URL = "jdbc:mysql://localhost:3306/mini_zoo";

    private final static String USER = "zoo_keeper";

    private final static String PASSWORD = "k33p3r";

    private final static String PARAMS = "?useUnicode=true"
            + "&useJDBCCompliantTimezoneShift=true"
            + "&useLegacyDatetimeCode=false"
            + "&serverTimezone=UTC";

    private static BasicDataSource ds;

//    public static Connection createConnection() throws SQLException {
//        Connection connection = DriverManager.getConnection(URL + PARAMS, USER, PASSWORD);
//        return connection;
//    }

    public static Connection createConnection() throws SQLException {
        if (ds == null) {
            ds = new BasicDataSource();
            ds.setUsername(USER);
            ds.setPassword(PASSWORD);
            ds.setUrl(URL + PARAMS);
        }
        Connection connection = ds.getConnection();
        return connection;
    }
}
