package pl.codementors.zoo.database;

import pl.codementors.zoo.model.Breeder;
import pl.codementors.zoo.model.Enclosure;
import pl.codementors.zoo.model.Species;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by psysiu on 6/29/17.
 */
public class EnclosuresDAO extends BaseDAO<Enclosure> {

    private String[] columns = {"name", "width", "height", "species"};

    public Enclosure create(String name, int width, int height, Species species) {
        return new Enclosure(name, width, height, species);
    }

    @Override
    public String getTableName() {
        return "enclosures";
    }

    @Override
    public Enclosure parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int width = result.getInt(3);
        int height = result.getInt(4);
        int speciesId = result.getInt(5);
        SpeciesDAO dao = new SpeciesDAO();
        Species species = dao.find(speciesId);
        return new Enclosure(id, name, width, height, species);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Enclosure value) {
        Object[] values = {value.getName(), value.getWidth(), value.getHeight(), value.getSpecies().getId()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Enclosure value) {
        return value.getId();
    }
}
