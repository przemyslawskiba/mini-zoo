package pl.codementors.zoo.database;

import pl.codementors.zoo.model.Animal;
import pl.codementors.zoo.model.Breeder;
import pl.codementors.zoo.model.Enclosure;
import pl.codementors.zoo.model.Species;

import java.sql.ResultSet;
import java.sql.SQLException;

/**
 * Created by pskiba on 29.06.2017.
 */
public class AnimalsDAO extends BaseDAO<Animal>{

    private String[] columns = {"name", "age", "gender", "breeder", "species", "enclosure"};



    @Override
    public String getTableName() {
        return "animals";
    }

    @Override
    public Animal parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        int age = result.getInt(3);
        String gender = result.getString(4);
        int breederId = result.getInt(5);
        int speciesId = result.getInt(6);
        int enclosureId = result.getInt(7);
        Species species = new SpeciesDAO().find(speciesId);
        Breeder breeder = new BreedersDAO().find(breederId);
        Enclosure enclosure = new EnclosuresDAO().find(enclosureId);
        return new Animal(id, name, age, Animal.Gender.valueOf(gender), breeder, species, enclosure);
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Animal value) {
        Object[] values = {value.getName(), value.getAge(), value.getGender().name(),
                value.getBreeder().getId(), value.getSpecies().getId(), value.getEnclosure().getId()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Animal value) {
        return value.getId();
    }
}

