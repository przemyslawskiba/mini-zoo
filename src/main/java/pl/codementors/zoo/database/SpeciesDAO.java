package pl.codementors.zoo.database;

import pl.codementors.zoo.model.Species;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by psysiu on 6/28/17.
 */
public class SpeciesDAO {

    public Species create(String name) {
        return new Species(name);
    }

    public void insert(Species species) {
        String sql = "INSERT INTO species (name) VALUES (?)";
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, species.getName());
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<Species> findALl() {
        String sql = "SELECT * FROM species";
        List<Species> species = new ArrayList<>();
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);
             ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                species.add(new Species(result.getInt(1), result.getString(2)));
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return species;
    }

    public void update(Species species) {
        String sql = "UPDATE species set name = ? WHERE id = ?";
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setString(1, species.getName());
            statement.setInt(2, species.getId());
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void delete(Species species) {
        String sql = "DELETE FROM species  WHERE id = ?";
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, species.getId());
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public Species find(int id) {
        String sql = "SELECT * FROM species WHERE id = ?";
        Species species = null;
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    species = new Species(result.getInt(1), result.getString(2));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return species;
    }

    public Species findByName(String name) {
        String sql = "SELECT * FROM species WHERE name = ?";
        Species species = null;
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);) {
            statement.setString(1, name);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    species = new Species(result.getInt(1), result.getString(2));
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return species;
    }

}
