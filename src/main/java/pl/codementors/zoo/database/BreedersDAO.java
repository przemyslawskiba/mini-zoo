package pl.codementors.zoo.database;

import pl.codementors.zoo.model.Breeder;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by psysiu on 6/29/17.
 */
public class BreedersDAO extends BaseDAO<Breeder> {

    private String[] columns = {"name", "surname", "position"};

    public Breeder create(String name, String surname, Breeder.Position position) {
        return new Breeder(name, surname, position);
    }

    @Override
    public String getTableName() {
        return "breeders";
    }

    @Override
    public String[] getColumns() {
        return columns;
    }

    @Override
    public Object[] getColumnsValues(Breeder value) {
        Object[] values = {value.getName(), value.getSurname(), value.getPosition().name()};
        return values;
    }

    @Override
    public int getPrimaryKeyValue(Breeder value) {
        return value.getId();
    }

    @Override
    public Breeder parseValue(ResultSet result) throws SQLException {
        int id = result.getInt(1);
        String name = result.getString(2);
        String surname = result.getString(3);
        String position = result.getString(4);
        return new Breeder(id, name, surname,
                position == null ? null : Breeder.Position.valueOf(position));
    }

    public Breeder findByName(String name, String surname) {
        String sql = "SELECT * FROM breeders WHERE name = ? AND surname = ?";
        Breeder breeder = null;
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);) {
            statement.setString(1, name);
            statement.setString(2, surname);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    breeder = parseValue(result);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return breeder;
    }

    public void delete(Breeder breeder) {
        delete(breeder.getId());
    }

}
