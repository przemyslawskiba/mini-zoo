package pl.codementors.zoo.database;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by psysiu on 6/29/17.
 */
public abstract class BaseDAO<T> {

    public abstract String getTableName();

    public abstract T parseValue(ResultSet result) throws SQLException;

    public abstract String[] getColumns();

    public abstract Object[] getColumnsValues(T value);

    public abstract int getPrimaryKeyValue(T value);

    private void setParams(PreparedStatement statement, Object[] values) throws SQLException {
        for (int i = 0; i < values.length; i++) {
            Object param = values[i];
            int paramIndex = i + 1;
            if (param instanceof String) {
                statement.setString(paramIndex, (String) param);
            } else if (param instanceof Integer) {
                statement.setInt(paramIndex, (Integer) param);
            } else {
                throw new IllegalArgumentException("Unsupported class " + param.getClass());
            }
        }
    }

    public void update(T value) {
        StringBuffer sql = new StringBuffer("UPDATE ");
        sql.append(getTableName()).append(" SET ");
        for (String column : getColumns()) {
            sql.append(column).append(" = ?, ");
        }
        sql.replace(sql.length() - 2, sql.length(), " ");
        sql.append("WHERE id = ?");
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql.toString())) {
            setParams(statement, getColumnsValues(value));
            statement.setInt(getColumns().length + 1, getPrimaryKeyValue(value));
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public void insert(T value) {
        StringBuffer sql = new StringBuffer("INSERT INTO ");
        sql.append(getTableName()).append(" (");
        for (String column : getColumns()) {
            sql.append(column).append(", ");
        }
        sql.replace(sql.length() - 2, sql.length(), ")");
        sql.append(" VALUES (");
        for (int i = 0; i < getColumns().length; i++) {
            sql.append("?, ");
        }
        sql.replace(sql.length() - 2, sql.length(), ")");
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql.toString())) {
            setParams(statement, getColumnsValues(value));
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public List<T> findALl() {
        String sql = "SELECT * FROM " + getTableName();
        List<T> values = new ArrayList<>();
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);
             ResultSet result = statement.executeQuery();) {
            while (result.next()) {
                T value = parseValue(result);
                values.add(value);
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return values;
    }

    public void delete(int id) {
        String sql = "DELETE FROM " + getTableName() + "  WHERE id = ?";
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql)) {
            statement.setInt(1, id);
            statement.execute();
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
    }

    public T find(int id) {
        String sql = "SELECT * FROM " + getTableName() + " WHERE id = ?";
        T value = null;
        try (Connection con = ConnectionFactory.createConnection();
             PreparedStatement statement = con.prepareStatement(sql);) {
            statement.setInt(1, id);
            try (ResultSet result = statement.executeQuery();) {
                if (result.next()) {
                    value = parseValue(result);
                }
            }
        } catch (SQLException ex) {
            ex.printStackTrace();
        }
        return value;
    }

}
