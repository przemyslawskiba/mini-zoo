package pl.codementors.zoo;

import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Species;

import java.util.List;
import java.util.Scanner;

/**
 * Created by psysiu on 6/29/17.
 */
public class SpeciesManager {

    private SpeciesDAO dao = new SpeciesDAO();

    public void manage(Scanner scanner) {
        System.out.print("command: ");
        String command = scanner.next();
        switch (command) {
            case "add": {
                add(scanner);
                break;
            }
            case "list": {
                list();
                break;
            }
            case "find": {
                find(scanner);
                break;
            }
            case "delete": {
                delete(scanner);
                break;
            }
            case "update": {
                update(scanner);
                break;
            }
        }
    }

    private void update(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        Species species = dao.findByName(name);
        System.out.println(species);
        if (species != null) {
            System.out.print("new name: ");
            name = scanner.next();
            species.setName(name);
            dao.update(species);
        }
    }

    private void delete(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        dao.delete(dao.findByName(name));
    }

    private void find(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        Species species = dao.findByName(name);
        System.out.println(species);
    }

    private void list() {
        List<Species> species = dao.findALl();
        for (Species s : species) {
            System.out.println(s);
        }
    }

    private void add(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        Species species = dao.create(name);
        dao.insert(species);
    }

}
