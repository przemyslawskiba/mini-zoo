package pl.codementors.zoo;

import pl.codementors.zoo.database.AnimalsDAO;
import pl.codementors.zoo.database.BreedersDAO;
import pl.codementors.zoo.database.EnclosuresDAO;
import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Animal;
import pl.codementors.zoo.model.Breeder;
import pl.codementors.zoo.model.Enclosure;
import pl.codementors.zoo.model.Species;

import java.util.List;
import java.util.Scanner;

/**
 * Created by pskiba on 29.06.2017.
 */
public class AnimalsManager {


    private AnimalsDAO dao = new AnimalsDAO();

    public void manage(Scanner scanner) {
        System.out.print("command: ");
        String command = scanner.next();
        switch (command) {
            case "add": {
                add(scanner);
                break;
            }
            case "list": {
                list();
                break;
            }
            case "find": {
                find(scanner);
                break;
            }
            case "delete": {
                delete(scanner);
                break;
            }
            case "update": {
                update(scanner);
                break;
            }
        }
    }

    private void update(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        Animal animal = dao.find(id);
        System.out.println(animal);
        if (animal != null) {
            System.out.print("new name: ");
            String name = scanner.next();
            System.out.print("new age: ");
            int age = scanner.nextInt();
            System.out.print("new gender: ");
            String gender = scanner.next();
            System.out.print("new species name: ");
            String speciesName = scanner.next();
            Species species = new SpeciesDAO().findByName(speciesName);
            System.out.print("new breeder name: ");
            String breederName = scanner.next();
            System.out.print("new breeder surname: ");
            String breederSurname = scanner.next();
            Breeder breeder = new BreedersDAO().findByName(breederName, breederSurname);
            System.out.print("new enclosure id: ");
            int enclosureId = scanner.nextInt();
            Enclosure enclosure = new EnclosuresDAO().find(enclosureId);
            animal.setName(name);
            animal.setAge(age);
            animal.setGender(Animal.Gender.valueOf(gender));
            animal.setBreeder(breeder);
            animal.setSpecies(species);
            animal.setEnclosure(enclosure);
            dao.update(animal);
        }
    }

    private void delete(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        dao.delete(id);
    }

    private void find(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        Animal animal = dao.find(id);
        System.out.println(animal);
    }

    private void list() {
        List<Animal> animals = dao.findALl();
        for (Animal s : animals) {
            System.out.println(s);
        }
    }

    private void add(Scanner scanner) {
        System.out.print("new name: ");
        String name = scanner.next();
        System.out.print("new age: ");
        int age = scanner.nextInt();
        System.out.print("new gender: ");
        String gender = scanner.next();
        System.out.print("new species name: ");
        String speciesName = scanner.next();
        Species species = new SpeciesDAO().findByName(speciesName);
        System.out.print("new breeder name: ");
        String breederName = scanner.next();
        System.out.print("new breeder surname: ");
        String breederSurname = scanner.next();
        Breeder breeder = new BreedersDAO().findByName(breederName, breederSurname);
        System.out.print("new enclosure id: ");
        int enclosureId = scanner.nextInt();
        Enclosure enclosure = new EnclosuresDAO().find(enclosureId);
        Animal animal = new Animal();
        animal.setName(name);
        animal.setAge(age);
        animal.setGender(Animal.Gender.valueOf(gender));
        animal.setBreeder(breeder);
        animal.setSpecies(species);
        animal.setEnclosure(enclosure);
        dao.insert(animal);
    }
}
