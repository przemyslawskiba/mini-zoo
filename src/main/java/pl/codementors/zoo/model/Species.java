package pl.codementors.zoo.model;

import lombok.*;

/**
 * Created by psysiu on 6/28/17.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode
@ToString
public class Species {

    private int id;

    private String name;

    public Species(String name) {
        this.name = name;
    }
}
