package pl.codementors.zoo;

import pl.codementors.zoo.database.EnclosuresDAO;
import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Enclosure;
import pl.codementors.zoo.model.Species;

import java.util.List;
import java.util.Scanner;

/**
 * Created by psysiu on 6/29/17.
 */
public class EnclosuresManager {

    private EnclosuresDAO dao = new EnclosuresDAO();

    public void manage(Scanner scanner) {
        System.out.print("command: ");
        String command = scanner.next();
        switch (command) {
            case "add": {
                add(scanner);
                break;
            }
            case "list": {
                list();
                break;
            }
            case "find": {
                find(scanner);
                break;
            }
            case "delete": {
                delete(scanner);
                break;
            }
            case "update": {
                update(scanner);
                break;
            }
        }
    }

    private void update(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        Enclosure enclosure = dao.find(id);
        System.out.println(enclosure);
        if (enclosure != null) {
            System.out.print("new name: ");
            String name = scanner.next();
            System.out.print("new width: ");
            int width = scanner.nextInt();
            System.out.print("new height: ");
            int height = scanner.nextInt();
            System.out.print("new species: ");
            String speciesName = scanner.next();
            Species species = new SpeciesDAO().findByName(speciesName);
            enclosure.setName(name);
            enclosure.setWidth(width);
            enclosure.setHeight(height);
            enclosure.setSpecies(species);
            dao.update(enclosure);
        }
    }

    private void delete(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        dao.delete(id);
    }

    private void find(Scanner scanner) {
        System.out.print("id: ");
        int id = scanner.nextInt();
        Enclosure enclosure = dao.find(id);
        System.out.println(enclosure);
    }

    private void list() {
        List<Enclosure> enclosures = dao.findALl();
        for (Enclosure s : enclosures) {
            System.out.println(s);
        }
    }

    private void add(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        System.out.print("width: ");
        int width = scanner.nextInt();
        System.out.print("height: ");
        int height = scanner.nextInt();
        System.out.print("species name: ");
        String speciesName = scanner.next();
        Species species = new SpeciesDAO().findByName(speciesName);
        Enclosure enclosure = dao.create(name, width, height, species);
        dao.insert(enclosure);
    }

}
