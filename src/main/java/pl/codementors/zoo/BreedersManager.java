package pl.codementors.zoo;

import pl.codementors.zoo.database.BreedersDAO;
import pl.codementors.zoo.database.SpeciesDAO;
import pl.codementors.zoo.model.Breeder;
import pl.codementors.zoo.model.Species;

import java.util.List;
import java.util.Scanner;

/**
 * Created by psysiu on 6/29/17.
 */
public class BreedersManager {

    private BreedersDAO dao = new BreedersDAO();

    public void manage(Scanner scanner) {
        System.out.print("command: ");
        String command = scanner.next();
        switch (command) {
            case "add": {
                add(scanner);
                break;
            }
            case "list": {
                list();
                break;
            }
            case "find": {
                find(scanner);
                break;
            }
            case "delete": {
                delete(scanner);
                break;
            }
            case "update": {
                update(scanner);
                break;
            }
        }
    }

    private void update(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        System.out.print("surname: ");
        String surname = scanner.next();
        Breeder breeder = dao.findByName(name, surname);
        System.out.println(breeder);
        if (breeder != null) {
            System.out.print("new name: ");
            name = scanner.next();
            System.out.print("new surname: ");
            surname = scanner.next();
            System.out.print("new position: ");
            String position = scanner.next();
            breeder.setName(name);
            breeder.setSurname(surname);
            breeder.setPosition(Breeder.Position.valueOf(position));
            dao.update(breeder);
        }
    }

    private void delete(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        System.out.print("surname: ");
        String surname = scanner.next();
        dao.delete(dao.findByName(name, surname));
    }

    private void find(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        System.out.print("surname: ");
        String surname = scanner.next();
        Breeder breeder = dao.findByName(name, surname);
        System.out.println(breeder);
    }

    private void list() {
        List<Breeder> breeders = dao.findALl();
        for (Breeder breeder : breeders) {
            System.out.println(breeder);
        }
    }

    private void add(Scanner scanner) {
        System.out.print("name: ");
        String name = scanner.next();
        System.out.print("surname: ");
        String surname = scanner.next();
        System.out.print("position: ");
        String position = scanner.next();
        Breeder breeder = dao.create(name, surname, Breeder.Position.valueOf(position));
        dao.insert(breeder);
    }

}
